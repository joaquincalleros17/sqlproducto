package com.example.sqlproducto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.DbProductos;
import database.Producto;

public class MainActivity extends AppCompatActivity {
    private Button btnNuevo;
    private Button btnAgregar;
    private Button btnLimpiar;
    private Button btnIrEditar;

    private DbProductos db;
    ArrayList<Producto> contactos = new ArrayList<>();
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton rbPerecedero;
    private RadioButton rbNoPerecedero;
    private Producto saveContact;
    private int savedIndex;
    private long indexActual;
    private Producto savedContact;
    private long id;
    private Producto producto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         btnNuevo = findViewById(R.id.btnNuevo);
         btnAgregar = findViewById(R.id.btnGuardar);
         btnLimpiar = findViewById(R.id.btnLimpiar);
         btnIrEditar = findViewById(R.id.btnIrEditar);

         txtCodigo = findViewById(R.id.txtCodigo);
         txtNombre = findViewById(R.id.txtNombre);
         txtMarca = findViewById(R.id.txtMarca);
         txtPrecio = findViewById(R.id.txtPrecio);
         rbPerecedero = findViewById(R.id.rdPerecedero);
         rbNoPerecedero = findViewById(R.id.rdNoPerecedero);

         producto = new Producto();


         btnAgregar.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(validarCampos() == false){
                     Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                 }else{
                     Producto nP = new Producto();
                     nP.setCodigo(Long.parseLong(txtCodigo.getText().toString()));
                     nP.setNombre(txtNombre.getText().toString());
                     nP.setMarca(txtMarca.getText().toString());
                     nP.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));
                     if(rbPerecedero.isChecked()){
                         nP.setDuracion(1);
                     }else{
                         nP.setDuracion(0);
                     }
                     db.openDataBase();
                     db.insertarProducto(nP);
                     Toast.makeText(MainActivity.this, "El Producto se agregó con éxito", Toast.LENGTH_SHORT).show();
                     limpiar();
                 }
             }
         });
         btnIrEditar.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(MainActivity.this,ProductoActivity.class);
                 Bundle objProducto = new Bundle();
                 objProducto.putSerializable("producto",producto);
                 intent.putExtras(objProducto);
                 startActivity(intent);
             }
         });
         btnLimpiar.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 limpiar();
             }
         });
         btnNuevo.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 txtCodigo.setSelection(0);
             }
         });
    }

    public boolean validarCampos(){
        if(txtCodigo.getText().toString().matches("")) return false;
        if(txtNombre.getText().toString().matches("")) return false;
        if(txtMarca.getText().toString().matches("")) return false;
        if(txtPrecio.getText().toString().matches("")) return false;
        return true;
    }
    public void limpiar(){
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        rbPerecedero.isChecked();
    }
}
