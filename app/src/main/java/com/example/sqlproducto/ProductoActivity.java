package com.example.sqlproducto;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.DbProductos;
import database.Producto;

public class ProductoActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton radioP;
    private RadioButton radioNP;
    private Button btnActualizar;
    private Button btnBuscar;
    private Button btnBorrar;
    private Button btnCerrar;

    private DbProductos db;
    private Producto producto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        radioP = findViewById(R.id.rdPerecedero);
        radioNP = findViewById(R.id.rdNoPerecedero);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnGuardar);
        btnCerrar = findViewById(R.id.btnCerrar);
        db = new DbProductos(this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                producto = db.getProducto(Long.parseLong(txtCodigo.getText().toString()));
                db.cerrarDataBase();

                if(producto != null) {
                    txtNombre.setText(producto.getNombre());
                    txtMarca.setText(producto.getMarca());
                    txtPrecio.setText(String.valueOf(producto.getPrecio()));
                } else
                {
                    Toast.makeText(ProductoActivity.this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
