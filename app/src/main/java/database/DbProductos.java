package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbProductos {
    private Context context;
    private ProductosDbHelper productosDbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[] {
            DefinirTabla.Producto.CODIGO,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.DURACION
    };


    public DbProductos(Context context){
        this.context = context;
        productosDbHelper = new ProductosDbHelper(this.context);
    }

    public void openDataBase(){ db = productosDbHelper.getWritableDatabase();}

    public long insertarProducto(Producto p){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO,p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE,p.getNombre());
        values.put(DefinirTabla.Producto.MARCA,p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO,p.getPrecio());
        values.put(DefinirTabla.Producto.DURACION,p.getDuracion());

        return db.insert(DefinirTabla.Producto.TABLA_NAME,null,values);
    }

    public long updateProducto(Producto p,long codigo){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO,p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE,p.getNombre());
        values.put(DefinirTabla.Producto.MARCA,p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO,p.getPrecio());
        values.put(DefinirTabla.Producto.DURACION,p.getDuracion());

        String criterio = DefinirTabla.Producto.CODIGO + " = " + codigo;

        return db.update(DefinirTabla.Producto.TABLA_NAME,values,criterio,null);
    }
    public int deleteContacto(long codigo){
        String criterio = DefinirTabla.Producto.CODIGO + " = " + codigo;
        return db.delete(DefinirTabla.Producto.TABLA_NAME,criterio,null);
    }
    public Producto readProducto(Cursor cursor){
        Producto p = new Producto();
        p.setCodigo(cursor.getLong(0));
        p.setNombre(cursor.getString(1));
        p.setMarca(cursor.getString(2));
        p.setPrecio(cursor.getFloat(3));
        p.setDuracion(cursor.getInt(4));
        return p;
    }

    public Producto getProducto(long codigo){
        SQLiteDatabase db = productosDbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLA_NAME,
                columnToRead,
                DefinirTabla.Producto.CODIGO + " =?",
                new String[] {String.valueOf(codigo)},
                null,
                null,
                null);
        if(cursor.moveToFirst()){
            Producto producto = readProducto(cursor);
            cursor.close();
            return producto;
        }else{
            return null;
        }
    }
    public void cerrarDataBase() {
        productosDbHelper.close();
    }
}
